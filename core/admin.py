from django.contrib import admin

from core.models import (
    Category,
    Cours,
    CoursFile
)

# Register your models here.

admin.site.register(Category)
admin.site.register(Cours)
admin.site.register(CoursFile)
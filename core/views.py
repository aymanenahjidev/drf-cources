from rest_framework.viewsets import ModelViewSet

from users.serializers import UserSerializer
from users.models import User
from core.models import (
    Category,
    Cours,
    CoursFile
)
from core.serializers import (
    CategorySerializer,
    CoursFileSerializer,
    CoursSerializer,
)

class CategoryViewSet(ModelViewSet):
    serializer_class=CategorySerializer
    queryset = Category.objects.all()
    
class CoursViewSet(ModelViewSet):
    serializer_class=CoursSerializer
    queryset = Cours.objects.all()

class CoursFileViewSet(ModelViewSet):
    serializer_class=CoursFileSerializer
    queryset = CoursFile.objects.all()
from rest_framework import serializers
from rest_flex_fields import FlexFieldsModelSerializer

from users.serializers import UserSerializer
from core.models import (
    Category,
    Cours,
    CoursFile
)

class CoursFileSerializer(FlexFieldsModelSerializer):
    class Meta:
        model=CoursFile
        fields=[
            'id',
            'title',
            'description',
            'file',
            'cours',
        ]

class CoursSerializer(FlexFieldsModelSerializer):
    files_count = serializers.IntegerField(read_only=True,source='coursfile_set.count')
    class Meta:
        model=Cours
        fields=[
            'id',
            'title',
            'description',
            'category',
            'files_count'
        ]

class CategorySerializer(FlexFieldsModelSerializer):
    class Meta:
        model=Category
        fields=[
            'id',
            'title',
            'description'
        ]




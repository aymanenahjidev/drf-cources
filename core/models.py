from django.db import models
from users.models import User

# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField(default='',blank=True)

    def __str__(self) -> str:
        return self.title

class Cours(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField(default='',blank=True)
    # user = models.ForeignKey(User,null=True,on_delete=models.SET_NULL)
    category = models.ForeignKey(Category,on_delete=models.PROTECT)

    def __str__(self) -> str:
        return f'{self.title} ({self.coursfile_set.count()})' 

class CoursFile(models.Model):
    title = models.CharField(max_length=256)
    description = models.TextField(default='',blank=True)
    file = models.FileField()
    cours = models.ForeignKey(Cours,on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f'{self.title} | {self.cours.title}' 


from rest_framework.routers import DefaultRouter

from core.views import (
    CategoryViewSet,
    CoursFileViewSet,
    CoursViewSet
)

router = DefaultRouter()

router.register('categories',CategoryViewSet,basename='categories')
router.register('courses',CoursViewSet,basename='courses')
router.register('cours-files',CoursFileViewSet,basename='cours-files')

urlpatterns = [
    
] + router.urls

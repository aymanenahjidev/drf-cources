from rest_framework import  permissions
from rest_framework.generics import RetrieveAPIView

from users.models import User
from users.serializers import UserSerializer
# Create your views here.

class UserViewSet(RetrieveAPIView):
    serializer_class=UserSerializer
    queryset = User.objects.all()
    permission_classes=[permissions.IsAuthenticated]

    def get_object(self):
        return self.request.user

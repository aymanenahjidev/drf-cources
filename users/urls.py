from django.urls import path,include
from rest_framework.routers import DefaultRouter

from users.views import UserViewSet

router = DefaultRouter()



urlpatterns = [
    path('me/',UserViewSet.as_view(),name='me')
] + router.urls

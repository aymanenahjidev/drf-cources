from rest_framework import serializers
from rest_flex_fields import FlexFieldsModelSerializer

from users.models import User

class UserSerializer(FlexFieldsModelSerializer):
    class Meta:
        model=User
        fields=[
            'username',
            'first_name',
            'last_name',
        ]